<?php
$email = $_POST['email'];
$error = $_POST['error'];
$to      = $email;
$subject = 'JS-RUNTIME-ERROR - New Error Captured';
$htmlMsg = '
<html>
<style>
    body {
        margin: 0;
        padding: 0;
        font-family: sans-serif;
    }

    center {
        background-color: rgba(233, 232, 232, 0.534);
        padding: 10px;
        margin: 0px;
    }

    center h1 {
        color: rgb(255, 0, 0);
    }

    .des {
        margin: 30px;
    }

    .error-stack {
        background-color: rgb(247, 209, 209);
        padding: 10px;
        margin: 50px;
        font-size: 12px;
    }
</style>

<body>
    <center>
        <h1>NGX Runtime Error</h1>
    </center>
    <br>
    <h3 class="des">We captured some error from your website! Please check and here are the logs</h3>
    <p class="error-stack">'.$error.'</p>
</body>

</html>
';
$message = $htmlMsg;
$headers = 'From: js-runtime-error@hostollo.net' . "\r\n" .
    'Reply-To: js-runtime-error@hostollo.net' . "\r\n" .
    'Content-Type: text/html' . "\r\n" .
    'MIME-Version: 1.0' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
if($email!=''){
    mail($to, $subject, $message, $headers);
    echo 'Mail Sent';
}else{
	echo 'Something Went Wrong!';
}
?> 