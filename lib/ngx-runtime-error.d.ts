export declare class NgxRuntimeError {
    private email;
    private errorArry;
    prodMode: any;
    constructor();
    /**
    * @param Config - Please Provide your Email Address so we can send Errors to your Email and ProdMode : you can use it for Production Error Message not for Developement.
    */
    init(params: {
        email: string;
        prodMode: boolean;
    }): void;
    captureExceptions(error: any): void;
    private APIcall;
}
