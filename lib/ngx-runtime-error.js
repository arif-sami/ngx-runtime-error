"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var NgxRuntimeError = /** @class */ (function () {
    function NgxRuntimeError() {
        this.email = '';
        this.errorArry = [];
    }
    /**
    * @param Config - Please Provide your Email Address so we can send Errors to your Email and ProdMode : you can use it for Production Error Message not for Developement.
    */
    NgxRuntimeError.prototype.init = function (params) {
        this.email = params.email;
        this.prodMode = params.prodMode;
    };
    NgxRuntimeError.prototype.captureExceptions = function (error) {
        if (this.email && this.prodMode) {
            var errorExist = this.errorArry.find(function (error) { return error.stack == error.stack; });
            if (!errorExist) {
                this.errorArry.push(error);
                this.APIcall();
            }
        }
    };
    NgxRuntimeError.prototype.APIcall = function () {
        var xhr = new XMLHttpRequest();
        var postParams = 'email=' + this.email + '&error=' + this.errorArry[this.errorArry.length - 1];
        xhr.open('POST', 'http://affan.hostollo.com/js-runtime-error/js-runtime-error.php');
        xhr.send(postParams);
    };
    return NgxRuntimeError;
}());
exports.NgxRuntimeError = NgxRuntimeError;
