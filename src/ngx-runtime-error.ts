export class NgxRuntimeError {
    private email: string = '';
    private errorArry: any = [];
    prodMode: any;
    constructor() { }

    /**
    * @param Config - Please Provide your Email Address so we can send Errors to your Email and ProdMode : you can use it for Production Error Message not for Developement.
    */
    public init(params: { email: string, prodMode: boolean }) {
        this.email = params.email;
        this.prodMode = params.prodMode
    }
    public captureExceptions(error: any) {
        if (this.email && this.prodMode) {
            let errorExist = this.errorArry.find((error:any) => error.stack == error.stack);
            if (!errorExist) {
                this.errorArry.push(error);
                this.APIcall();
            }

        }
    }
    private APIcall() {
        var xhr = new XMLHttpRequest();
        var postParams = new FormData();
        postParams.append('email',this.email);
        postParams.append('error',this.errorArry[this.errorArry.length-1].stack);
        xhr.open('POST', 'http://affan.hostollo.com/js-runtime-error/js-runtime-error.php');
        xhr.send(postParams);
    }
}